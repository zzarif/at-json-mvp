package com.example.taskjsonmvp.model;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.taskjsonmvp.MainContract;
import com.example.taskjsonmvp.presenter.MainPresenter;
import com.example.taskjsonmvp.view.MainView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class MainModel implements MainContract.Model {
    MainPresenter mainPresenter;
    String url;
    RequestQueue requestQueue;
    JsonObjectRequest request;
    public ArrayList<String> teacher;
    public ArrayList<String> institution;

    public MainModel (MainPresenter mainPresenter) {
        this.mainPresenter = mainPresenter;
        teacher = new ArrayList<>();
        institution = new ArrayList<>();
    }

    public void setRequestQueue (RequestQueue requestQueue) {
        this.requestQueue = requestQueue;
    }

    public void jsonParse (String[] cities) {
        int i;
        for (i=0; i<cities.length-1; ++i) {
            url = "https://api.openweathermap.org/data/2.5/weather?q=" + cities[i] + "&appid=bf7c53664f16e111935e830749684e22";
            request = new JsonObjectRequest(Request.Method.GET, url, null,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                JSONArray jsonArray = response.getJSONArray("weather");

                                JSONObject theTeacher = jsonArray.getJSONObject(0);

                                String name = theTeacher.getString("main");
                                String institute = theTeacher.getString("description");

                                teacher.add(name);
                                institution.add(institute);


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                }
            });

            requestQueue.add(request);
        }

        url = "https://api.openweathermap.org/data/2.5/weather?q=" + cities[i] + "&appid=bf7c53664f16e111935e830749684e22";
        request = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        mainPresenter.adapter.notifyDataSetChanged();
                        try {
                            JSONArray jsonArray = response.getJSONArray("weather");

                            JSONObject theTeacher = jsonArray.getJSONObject(0);

                            String name = theTeacher.getString("main");
                            String institute = theTeacher.getString("description");

                            teacher.add(name);
                            institution.add(institute);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });

        requestQueue.add(request);
    }
}
