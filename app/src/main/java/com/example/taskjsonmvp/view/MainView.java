package com.example.taskjsonmvp.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.transition.AutoTransition;
import android.transition.TransitionManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.taskjsonmvp.presenter.Adapter;
import com.example.taskjsonmvp.MainContract;
import com.example.taskjsonmvp.R;
import com.example.taskjsonmvp.presenter.MainPresenter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MainView extends AppCompatActivity implements MainContract.View {
    MainPresenter mainPresenter;

    CardView cardView;
    public RecyclerView recyclerView;
    public LinearLayout button;
    public TextView buttonText;
    public ImageView buttonArrow;
    public LinearLayout.LayoutParams params;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        cardView = findViewById(R.id.cardView);
        recyclerView = findViewById(R.id.recyclerView);
        button = findViewById(R.id.button);
        buttonText = findViewById(R.id.buttonText);
        buttonArrow = findViewById(R.id.buttonArrow);

        mainPresenter = new MainPresenter(this);

        mainPresenter.setRequestQueue();
        mainPresenter.setTeacherData();

        mainPresenter.setAdapterViews();
        recyclerView.setAdapter(mainPresenter.adapter);

        mainPresenter.initRecyclerViewParams();

        button.setOnClickListener(new android.view.View.OnClickListener() {

            @Override
            public void onClick(android.view.View view) {
                TransitionManager.beginDelayedTransition(cardView, new AutoTransition());
                mainPresenter.onClick();
            }
        });
    }
}
