package com.example.taskjsonmvp;

import android.os.Bundle;

import com.android.volley.RequestQueue;

public interface MainContract {

    interface View {

    }

    interface Presenter {
        void initRecyclerViewParams ();
        void onClick ();
        void setTeacherData ();
        void setAdapterViews ();
        void setRequestQueue ();
    }

    interface Model {
        void setRequestQueue (RequestQueue requestQueue);
        void jsonParse (String[] cities);
    }
}
