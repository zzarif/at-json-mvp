package com.example.taskjsonmvp.presenter;

import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.recyclerview.widget.GridLayoutManager;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.example.taskjsonmvp.MainContract;
import com.example.taskjsonmvp.model.MainModel;
import com.example.taskjsonmvp.view.MainView;
import com.example.taskjsonmvp.R;

import java.util.ArrayList;

public class MainPresenter implements MainContract.Presenter {

    final String hide = "Hide";
    final String seeMoreTeachers = "See More Teachers";
    final int column = 3;

    public MainView mainView;
    MainModel mainModel;
    public Adapter adapter;
    public ArrayList<String> teacher;
    public ArrayList<String> institution;
    public RequestQueue requestQueue;
    int initHeight;
    String[] cities;

    public MainPresenter (MainView mainView) {
        this.mainView=mainView;
        teacher = new ArrayList<>();
        institution = new ArrayList<>();
    }

    public void initRecyclerViewParams () {
        mainView.params = (LinearLayout.LayoutParams) mainView.recyclerView.getLayoutParams();
        initHeight=mainView.params.height;
    }

    public void onClick () {
        if (mainView.params.height == initHeight) {
            mainView.params.height= ViewGroup.LayoutParams.WRAP_CONTENT;
            mainView.recyclerView.setLayoutParams(mainView.params);
            mainView.buttonText.setText(hide);
            mainView.buttonArrow.setBackgroundResource(R.drawable.ic_baseline_keyboard_arrow_up_21);
        }
        else {
            mainView.params.height = initHeight;
            mainView.recyclerView.setLayoutParams(mainView.params);
            mainView.buttonText.setText(seeMoreTeachers);
            mainView.buttonArrow.setBackgroundResource(R.drawable.ic_baseline_keyboard_arrow_down_20);
        }
    }

    public void setTeacherData () {
        mainModel = new MainModel(this);
        mainModel.setRequestQueue(requestQueue);
        cities = new String[]{"london", "dhaka", "serbia", "islamabad", "paris"};
        mainModel.jsonParse(cities);
        teacher = mainModel.teacher;
        institution = mainModel.institution;
    }

    public void setAdapterViews () {
        mainView.recyclerView.setLayoutManager(new GridLayoutManager(mainView, column));
        adapter = new Adapter(teacher, institution);
    }

    public void setRequestQueue () {
        this.requestQueue = Volley.newRequestQueue(mainView);
    }
}
