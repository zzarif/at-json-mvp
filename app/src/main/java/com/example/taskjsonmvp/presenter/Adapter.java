package com.example.taskjsonmvp.presenter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.taskjsonmvp.R;

import java.util.ArrayList;
import de.hdodenhof.circleimageview.CircleImageView;

public class Adapter extends RecyclerView.Adapter<Adapter.ViewHolder> {

    ArrayList<String> teacherName;
    ArrayList<String> instituteName;

    public Adapter (ArrayList<String> teacherName, ArrayList<String> instituteName) {
        this.teacherName=teacherName;
        this.instituteName=instituteName;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.layout_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.ivImageView.setImageResource(R.drawable.ic_baseline_account_circle_20);
        holder.tvTeacherName.setText(teacherName.get(position));
        holder.tvInstitutionName.setText(instituteName.get(position));
    }

    @Override
    public int getItemCount() {
        return teacherName.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        CircleImageView ivImageView;
        TextView tvTeacherName;
        TextView tvInstitutionName;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            ivImageView = itemView.findViewById(R.id.ivImageView);
            tvTeacherName = itemView.findViewById(R.id.tvTeacherName);
            tvInstitutionName = itemView.findViewById(R.id.tvInstitutionName);
        }
    }
}
